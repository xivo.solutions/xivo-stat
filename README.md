xivo-stat
=========
[![Build Status](https://travis-ci.org/xivo-pbx/xivo-stat.png?branch=master)](https://travis-ci.org/xivo-pbx/xivo-stat)

XiVO statistic generation utilities


Running unit tests
------------------

```
apt-get install libpq-dev python-dev libyaml-dev
pip install tox
tox --recreate -e py27
```
